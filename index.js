const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");

const spanFullName = document.querySelector("#span-full-name");

txtFirstName.addEventListener('input', updateSpanFullName);
txtLastName.addEventListener('input', updateSpanFullName);

function updateSpanFullName() {
	const firstName = txtFirstName.value;
	const lastName = txtLastName.value;
	const fullName = `${firstName} ${lastName}`;

	spanFullName.innerHTML = fullName;
};


